"""
Calculates TF-IDF

Author William Arias

app v 1.0

"""

from sklearn.feature_extraction.text import TfidfVectorizer
from whatlies.language import SpacyLanguage
from collections import defaultdict
import ast
import numpy as np
from whatlies.transformers import Pca


def tf_idf(text):
    """ Ranks the words and sentences using TF-IDF
    1. Create Vector representation of the text Object
    2. Pass the text to the object fit and transform it to TF-IDF values
    3. Convert TF-IDF Object to ndarray
    4. Sum up along the whole corpus tf idf values columns it changes the shape to unidimensional tuple
       and gives the total contribution to the corpus (number)
    4. Get top results
    """

    stop_words_idf = ['amp', '39', '01', '00', '0001', '10000', 'gitlab', 'gitlab ']
    vectorizer = TfidfVectorizer(ngram_range=(1, 1), stop_words=stop_words_idf)
    text_tfidf = vectorizer.fit_transform(text).toarray()
    features = vectorizer.get_feature_names_out()
    sums = text_tfidf.sum(axis=0)
    ranking_data = []
    for column, term in enumerate(features):
        ranking_data.append((term, sums[column]))

    return ranking_data


def embeddings(corpus):
    """language backend used to fetch byte-pair embeddings
    input: corpus made of tokens
    Output: embeddings for each token using Byte-Pair"""
    corpus = list(set(corpus))
    language = SpacyLanguage("en_core_web_sm")
    return language[corpus]


def token_counter(tokens_column):
    """language backend used to fetch byte-pair embeddings
        input: corpus made of tokens
        Output: embeddings for each token using Byte-Pair"""

    tokens_list = tokens_column.to_numpy()
    tokens_corpus = [word for token_sublist in tokens_list for word in ast.literal_eval(token_sublist)]
    count_token_dict = defaultdict()

    for token in tokens_corpus:
        if token in count_token_dict:
            count_token_dict[token] += 1
        else:
            count_token_dict[token] = 1

    return count_token_dict


def rank_counts(corpus_dict):
    """language backend used to fetch byte-pair embeddings
        input: corpus made of tokens
        Output: embeddings for each token using Byte-Pair"""
    sc_words = {key: value for key, value in sorted(corpus_dict.items(), key=lambda element: element[1], reverse=True)}
    return sc_words
