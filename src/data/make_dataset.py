import ast


def dataset_maker(corpus_dataframe=None):
    corpus = corpus_dataframe.to_numpy()
    corpus = [word for word_list in corpus for word in ast.literal_eval(word_list)]
    return corpus