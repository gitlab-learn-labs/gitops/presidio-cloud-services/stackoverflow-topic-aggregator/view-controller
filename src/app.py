from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import streamlit as st
import matplotlib.pyplot as plt
import pandas as pd
from wordcloud import WordCloud

st.title('SO questions to GitLab Collective')

# import our data

DATASET = '../data/interim/so-questions-preproc.csv'

df = pd.read_csv(DATASET)
df_qs = df.copy()
df_qs.drop(['Unnamed: 0'], axis=1)
#st.write(questions_df.head(10))


stop_words = ['there', 'how', 'to', '39', '01', '00']
vectorizer = TfidfVectorizer(ngram_range=(4,4), stop_words=stop_words)
titles_tfidf = vectorizer.fit_transform(df_qs['title_prc'])
features = (vectorizer.get_feature_names())
titles_tfidf = titles_tfidf.toarray()

# Getting top ranking features
sums = titles_tfidf.sum(axis=0) # summing up the whole corpus

# gets a vector with the value of each tri gram along all the documents

data = []
for col, term in enumerate(features):
    data.append((term, sums[col]))
ranking = pd.DataFrame(data, columns = ['ngram', 'rank'])
words = (ranking.sort_values('rank', ascending = False))

st.write("Top n-grams : \n", words.head(10))

wc_txt = " ".join(title for title in words.ngram.astype(str))
stop_words_wc = ['gitlab', 'how', 'to', 'the']
word_cloud = WordCloud(stopwords=stop_words_wc, background_color='white', min_word_length=3, max_words=500, width=1000, height=500)
word_cloud.generate_from_text(wc_txt)
word_cloud.to_file('word_cloud.png')
st.text("WordCloud Generated")
st.image("word_cloud.png")
